# Improvement Planner

##Important
I was not able to show you the original project linked as it was connected to my company's website. However, I have extracted all the relevant code in a similar, smaller project so you can review my work.

## Getting started
If you're not too familiar with angular and want to check out my work then the best place to check it out is in **src/app** and then navigate to the **/components**, **/models** and **/services** folders. These house the majority of the work.

 - If you want to see my css you'll find it in each components folder e.g. **app/components/team-details/team-details.component.scss**
 - The universal styles folder can be found in **src/sass/styles.scss**

##Running the code
If you want to actually test the code then:

 - Make sure angular is installed
`npm install -g @angular/cli` 
 - Clone this project
`git clone https://kevinburn@bitbucket.org/kevinburn/improvement-planner.git`
 - Navigate to the project and get all dependencies with
`npm install`
 - Run the code on a dev server using
`npm start`
 - Navigate to localhost:4200 once the dev server has started
