import { Component } from '@angular/core';
import {TeamworkComponent } from './components/teamwork/teamwork.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
}
