import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { TeamworkComponent } from './components/teamwork/teamwork.component';
import { TeamGraphComponent } from './components/team-graph/team-graph.component';
import { TeamFiltersComponent } from './components/team-filters/team-filters.component';
import { TeamDetailsComponent } from './components/team-details/team-details.component';
import { TeamListComponent } from './components/team-list/team-list.component';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent,
    TeamworkComponent,
    TeamGraphComponent,
    TeamFiltersComponent,
    TeamDetailsComponent,
    TeamListComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
