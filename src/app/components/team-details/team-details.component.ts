import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Improvement } from '../../models/improvement';
@Component({
  selector: 'team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TeamDetailsComponent implements OnInit {

  @Input() improvement: Improvement;

  constructor() { }

  ngOnInit() {
  }

}
