import { Component, OnInit, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'team-filters',
  templateUrl: './team-filters.component.html',
  styleUrls: ['./team-filters.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TeamFiltersComponent implements OnInit {
  private _filters: Array<string> = ["impact", "complexity", "application", "type"];
  private _currentFilter: string = "application";
  @Output() filterUpdated = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.filterSelected(this._currentFilter);
  }

  updateSelectedTags() {

  }

  filterSelected(filter) {
    this._currentFilter = filter;
    this.filterUpdated.emit(filter);
  }
}
