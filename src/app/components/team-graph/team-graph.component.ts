import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import {Chart} from 'chart.js';
import {Improvement} from '../../models/improvement';
import { ImprovementManagerService } from '../../services/improvement-manager/improvement-manager.service';

import {Observable} from 'rxjs/Observable'

/// <reference path="browser/main.d.ts" />

@Component({
  selector: 'team-graph',
  templateUrl: './team-graph.component.html',
  styleUrls: ['./team-graph.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ImprovementManagerService]
})
export class TeamGraphComponent implements OnInit {
  private _improvements: Array<Improvement> = [];
  private _filter: string = "";
  private _chart: any = null;
  private _viewInited = false;
  private _improvementsLoaded = false;
  private _currentBreakdown: any = null;

  @Input('improvements')
  set improvements(value: Array<Improvement>) {
    this._improvements = value;
    if(this._improvements.length > 0) {
      this.onImprovementsLoaded();
    }
  }
  @Input('filter')
  set filter(value: string) {
    this._filter = value;
    this.onFilterUpdated();
  }

  constructor(private improvementManager: ImprovementManagerService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this._viewInited = true;
    //this.stageWindow = new createjs.Stage(this.canvas);
    if(this._improvementsLoaded) {
      this.createChart();
    }
  }

  onImprovementsLoaded() {
    this._improvementsLoaded = true;
    this.onFilterUpdated();//We've got new improvements so we should reapply our filter
    if(this._viewInited) {
      this.createChart();
    }
  }

  onFilterUpdated() {
    this._currentBreakdown = this.improvementManager.getImprovementsBy(this._improvements, this._filter);
    if(this._viewInited && this._improvementsLoaded) {
      this.createChart();
    }
  }

  createChart() {
    var canvas = <HTMLCanvasElement>document.getElementById("myChart");
    var ctx = canvas.getContext('2d');
    var labels = [];
    var data = [];
    //Takes the key values from the current breakdown and uses them as the chart's labels
    //Counts the number of values at each key and uses this as our data for the chart
    if(this._currentBreakdown != null) {
      for(var key in this._currentBreakdown) {
        labels.push(key);
        data.push(this._currentBreakdown[key].length);
      }
    }

    if(this._chart != null) {
      this._chart.destroy();
    }
    this._chart = new Chart(ctx, {
      type: 'pie',
      data: {
        labels:  labels,
        datasets: [{
            data: data,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(54, 162, 235, 0.2)',
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(75, 192, 192, 1)',
                'rgba(54, 162, 235, 1)',
            ],
            borderWidth: 2
        }]
      },
      options: {
          cutoutPercentage: 50
      }
    });

    var that = this;
    document.getElementById("myChart").onclick = function(evt){
        //Get point(s) of chart that were clicked
        var activePoints = that._chart.getElementsAtEvent(evt);
        if(activePoints.length > 0) {
          //get the internal index of slice in pie chart
          var clickedElementindex = activePoints[0]["_index"];
          var label = that._chart.data.labels[clickedElementindex];
          var value = that._chart.data.datasets[0].data[clickedElementindex];
          //TODO: Allow filtering on individual elements of labels
          //e.g. Filter on impact and then be able to select "Low" part of chart to only show low impact in team-list
       }
    };
  }
}
