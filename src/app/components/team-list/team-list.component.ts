import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { ImprovementManagerService } from '../../services/improvement-manager/improvement-manager.service';
import { Improvement } from '../../models/improvement';

@Component({
  selector: 'team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ImprovementManagerService]
})
export class TeamListComponent implements OnInit {
  private _filter: string = null;
  private _improvements: Array<Improvement> = null;
  private _sortedImprovements: {} = null;
  private _currentImprovement: Improvement = null;
  private _objKeys: any;//Reference to ES2015 Object.keys function

  @Output() currentImprovementUpdated = new EventEmitter();
  @Input('improvements')
  set improvements(value: Array<Improvement>) {
    this._improvements = value;
    if(this._filter != null) {
      this._sortedImprovements = this._impManager.getImprovementsBy(value, this._filter);
    }
  }
  @Input('filter')
  set filter(value: string) {
    this._filter = value;
    if(this._improvements != null) {
      this._sortedImprovements = this._impManager.getImprovementsBy(this._improvements, value);
    }
  }

  constructor(private _impManager: ImprovementManagerService) {
    this.polyfill();
    this._objKeys = Object.keys;
  }

  ngOnInit() {
  }

  /**
  * Provides polyfill for Object.keys function
  */
  polyfill() {
    // From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
    if (!Object.keys) {
      Object.keys = (function() {
        'use strict';
        var hasOwnProperty = Object.prototype.hasOwnProperty,
            hasDontEnumBug = !({ toString: null }).propertyIsEnumerable('toString'),
            dontEnums = [
              'toString',
              'toLocaleString',
              'valueOf',
              'hasOwnProperty',
              'isPrototypeOf',
              'propertyIsEnumerable',
              'constructor'
            ],
            dontEnumsLength = dontEnums.length;

        return function(obj) {
          if (typeof obj !== 'function' && (typeof obj !== 'object' || obj === null)) {
            throw new TypeError('Object.keys called on non-object');
          }

          var result = [], prop, i;

          for (prop in obj) {
            if (hasOwnProperty.call(obj, prop)) {
              result.push(prop);
            }
          }

          if (hasDontEnumBug) {
            for (i = 0; i < dontEnumsLength; i++) {
              if (hasOwnProperty.call(obj, dontEnums[i])) {
                result.push(dontEnums[i]);
              }
            }
          }
          return result;
        };
      }());
    }
  }

  /**
  * Returns the correct class for the list's title depending on the given value
  * NOTE: Was needed because can't inline default in NgClass directive
  */
  selectTitleClass(value) {
    if(value=='chat')
      return 'cat-chat';
    else if(value=='desk' || value=='feature')
      return 'cat-desk';
    else if(value == 'projects' || value=='improvement')
      return 'cat-projects';
    return 'cat-default';
  }

  updateImprovement(improvement) {
    this._currentImprovement = improvement;
    this.currentImprovementUpdated.emit(improvement);
  }
}
