import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TeamGraphComponent } from '../team-graph/team-graph.component';
import { TeamFiltersComponent } from '../team-filters/team-filters.component';
import { TeamDetailsComponent } from '../team-details/team-details.component';
import { TeamListComponent } from '../team-list/team-list.component';

import { ImprovementsLoaderService } from '../../services/improvements-loader/improvements-loader.service';
import { Improvement } from '../../models/improvement';
@Component({
  selector: 'team-teamwork',
  templateUrl: './teamwork.component.html',
  providers: [ImprovementsLoaderService],
  styleUrls: ['./teamwork.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TeamworkComponent implements OnInit {
  private _improvements: Array<Improvement> = [];
  private _currentImprovement = null;
  private _currentFilter: string = "";

  constructor(private _improvementsLoader: ImprovementsLoaderService) {
    var that = this;
    this._improvementsLoader.improvementsLoaded.subscribe(function(res: Array<Improvement>) {
      that._improvements = res;
    });
    this._improvementsLoader.loadImprovements();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {

  }

  updateCurrentImprovement(improvement) {
    this._currentImprovement = improvement;
  }

  updateFilter(filter) {
    this._currentFilter = filter;
  }
}
