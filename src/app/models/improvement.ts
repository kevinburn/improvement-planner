export class Improvement implements IImprovement {
  application: string = "";
  title: string = "";
  description: string = "";
  why: string = "";
  images: string[]= [];
  impact: number = 0;
  complexity: number = 0;
  types: string[] = []
  tags: string[] = [];
}

interface IImprovement {
  application: string;
  title: string;
  description: string;
  why: string;
  images: string[];
  impact: number;
  complexity: number;
  types: string[];
  tags: string[];
}
