import { TestBed, inject } from '@angular/core/testing';

import { ImprovementManagerService } from './improvement-manager.service';

describe('ImprovementManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ImprovementManagerService]
    });
  });

  it('should be created', inject([ImprovementManagerService], (service: ImprovementManagerService) => {
    expect(service).toBeTruthy();
  }));
});
