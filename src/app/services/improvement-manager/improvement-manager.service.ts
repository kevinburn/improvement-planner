import { Injectable } from '@angular/core';
import { Improvement } from '../../models/improvement';

@Injectable()
export class ImprovementManagerService {

  constructor() { }

  /**
  * Returns the names of the different attributes of improvements that we can
  * filter/breakdown by along with the labels associated with each
  */
  getImprovementBreakdown() : any {
    return {//Could also load this from json
      "impact": {"labels":["Low", "Medium", "High"], "ranges":[3, 6, 10], "ranged": true },
      "complexity": {"labels":["Low", "Medium", "High"], "ranges":[3, 6, 10], "ranged": true },
      "application":{"labels": ["chat","desk", "projects"] },
      "type":{"labels":["improvement", "feature"]}
    };
  }

  /**
  * Returns the given list of improvements divided by the given attribute
  * according to the rules set by the improvement breakdown
  * @returns An object containing all labels associated with the given attribute whose values will correspond to improvements with those labels
  */
  getImprovementsBy(improvements, attribute) {
    var breakdown = this.getImprovementBreakdown();
    if(breakdown[attribute] == undefined) {
      return {};
    }

    if(breakdown[attribute]["ranged"] == true) {
      return this._getImprovementsByRange(improvements, attribute, breakdown[attribute]["labels"], breakdown[attribute]["ranges"])
    }
    return this._getImprovementsByValue(improvements, attribute, breakdown[attribute]["labels"]);
  }

  _getImprovementsByRange(improvements, attribute, labels, ranges){
    var ret = {}
    //Setup each label as a key to an empty array that we can push our improvements to
    for(var i = 0; i < labels.length; i++) {
      ret[labels[i]] = [];
    }

    for(var i = 0; i < improvements.length; i++) {
      if(typeof improvements[i][attribute] == 'number') {
        for(var j = 0; j < ranges.length; j++) {
          if(improvements[i][attribute] <= ranges[j]) {
            ret[labels[j]].push(improvements[i]);
            break;
          }
        }
      }
    }

    return ret;
  }

  _getImprovementsByValue(improvements, attribute, labels) {
    var ret = {}
    //Setup each label as a key to an empty array that we can push our improvements to
    for(var i = 0; i < labels.length; i++) {
      ret[labels[i]] = [];
    }

    for(var i = 0; i < improvements.length; i++) {
      for(var j = 0; j < labels.length; j++) {
        if(improvements[i][attribute] == labels[j]) {

          /*console.log("ret[labels[j]]");
          console.log(ret[labels[j]]);
          console.log(ret);*/
          ret[labels[j]].push(improvements[i]);
          break;
        }
      }
    }
    return ret;
  }
}
