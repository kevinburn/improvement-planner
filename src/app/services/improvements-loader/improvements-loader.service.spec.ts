import { TestBed, inject } from '@angular/core/testing';

import { ImprovementsLoaderService } from './improvements-loader.service';

describe('ImprovementsLoaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ImprovementsLoaderService]
    });
  });

  it('should be created', inject([ImprovementsLoaderService], (service: ImprovementsLoaderService) => {
    expect(service).toBeTruthy();
  }));
});
