import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Improvement } from '../../models/improvement';

@Injectable()
export class ImprovementsLoaderService {  private improvements: Array<Improvement> = [];
  improvementsLoaded: EventEmitter<Array<Improvement>> = new EventEmitter();
  improvementsLoadFail: EventEmitter<string> = new EventEmitter();

  //improvementsUpdated: EventEmitter<Array<Improvement>> = new EventEmitter();

  constructor(private http: Http) {
  }

  loadImprovements() {
    var that = this;
    this.http.get("assets/teamwork/improvements.json").subscribe((value) => {
      that.improvementsLoaded.emit(value.json());
    }, (err) => {
      that.improvementsLoadFail.emit(err);
    });
  }
}
